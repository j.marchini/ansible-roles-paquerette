# monitoring of an application

- vars:
  - monit_request: http request, by default : **app_domain**
  - monit_expect: expected string expression in http response (regular expression)
  - monit_timeout : **3**

uses 
- [monit](https://mmonit.com/documentation/)
- http_check utility (see templates)

[paquerette.eu](http://paquerette.eu)
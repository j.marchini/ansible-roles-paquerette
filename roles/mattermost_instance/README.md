# mattermost instance 

- vars : 
    - **app_domain**
    - **app_instance_id**
    - **app_version**
    - **app_old_version** : last app_version
    - **app_run** : skip / install / reinstall / upgrade
    - app_port : **8065** 

- platform roles : 
    - apache2 / nginx
    - postgres_server

[paquerette.eu](http://paquerette.eu)

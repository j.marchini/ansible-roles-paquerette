# Utility role : cleanup old domain name for an application 

- vars:
  - **app_domain**: full old domain name for the service

- removes and revoke letsencrypt certificate for old domain

usage 

requirements : 
 - role : None

[paquerette.eu](http://paquerette.eu)
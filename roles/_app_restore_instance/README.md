# restore application files and database for the instance

  *list*: list tar archive files containing instance, the result is usefull for restore, recover or transfer

  *recover*: fetch locally (on the controller) an archive of the instance from **restore_source**

  *transfer*: copy an archive of the instance from host/**restore_source** to **transfer_dest** (same format as recover)

  *restore*: restore application files and database from "host / **restore_source**" 

- vars:

  - **app_instance_id**
  - **restore_action**: list / recover / transfer / restore   

  mandatory for list action :
  if listing on master backup host, use **app_host** to specify the instance's host
  
  mandatory for restore, transfer and recover actions :
  - **restore_source**, the archive file location. use *list* to list all sources available on host or master backup host 

  optional for restore action :
  - from_instance_id:  restores files and database with files and database from a different instance 
  - from_full_archive: **yes**/no change to "no" if the archive contains only the instance (after recover or transfer). The "full archive" contains all host's instances.
  - from_version_backup: yes/**no** set to "yes" to restore from unarchived directory created by [automatic backup](../_app_backup_instance/README.md) when upgrading the app
  - from_directory: yes/**no** set to yes to read from restore_source without the archive extraction step
  
[paquerette.eu](http://paquerette.eu)
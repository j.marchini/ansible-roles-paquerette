# Collabora Online Instance : 

- for Collabora partners with partner key
- based on collabora packages so only one instance is allowed on a single server

- vars : 
    - collabora_version: **4**
    - collabora_distro_repo: **ubuntu1804** / ubuntu1604...
    - **collabora_domain**
    - **collabora_client_domains** : list of client domains
    - collabora_port: **9980**


- platform roles : 
    - apache2 / nginx
    
to run install : 
./play.py -r myhost collabora_online_instance 

to run reinstall :
./play.py -r myhost collabora_online_instance -e 'app_run=reinstall'
 

[paquerette.eu](http://paquerette.eu)
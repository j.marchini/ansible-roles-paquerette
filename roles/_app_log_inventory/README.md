# backup task for application and database

- vars:
  - **log_type**: install / reinstall / upgrade / uninstall / server
  - app_instance_to_uninstall: mandatory for uninstall, il non defined, assertion fails


to avoid collision on installation for instances:
if app_instance_id is found in inventory and app_run is install then assertion fails
if app_instance_id is not found in inventory and app_run is reinstall then assertion fails

to confirm uninstall, \<app_instance_to_uninstall\> must be set with \<app_instance_id\> value


[paquerette.eu](http://paquerette.eu)
# tryton instance 

- vars : 
    - **app_domain**
    - **app_instance_id**
    - **app_version**
    - **app_old_version** : last app_version
    - **app_run** : skip / install / reinstall / upgrade / restore
    - **database_password** 
    - **admin_email**
    - default_admin_password : **admin**
    - web_client: yes/**no** : whether sao is installed or not (requires NodeJS) 

- platform roles : 
    - apache2 
    - postgres_server
    - when web client is installed : nodejs 
    
[paquerette.eu](http://paquerette.eu)
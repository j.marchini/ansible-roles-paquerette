# Basic configuration of server : 

- file system organization
- locales
- firewall
- mailing configuration
- monitoring
- backup strategy
- user group for sftp only in chroot : {{ sftp_users_chroot }}

requirements : 
 - role : None
 - inventory : group base_server ( and secrets ... )

[paquerette.eu](http://paquerette.eu)
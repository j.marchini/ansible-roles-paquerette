# rocket.chat instance 

- vars : 
    - **app_domain**
    - **app_instance_id**
    - app_version : **latest**
    - **app_old_version** : last app_version
    - **app_run** : skip / install / reinstall / upgrade / restore
    - app_port : **3001** 

- platform roles : 
    - apache2 / nginx
    - nodejs
    - mongodb

[paquerette.eu](http://paquerette.eu)
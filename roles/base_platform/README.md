# Configuration of platform : 

- runs server components roles according to host vars inventory
- available components :
    - apache2_server
    - nginx_server
    - mongodb_server
    - postgres_server
    - mariadb_mysql_server
    - nodejs
    - php7_fpm

requirements : 
- role : base_server
- inventory : host vars
  
[paquerette.eu](http://paquerette.eu)
Installation sécurisée complete
* Turn server
* https://github.com/coturn/coturn/blob/master/INSTALL
* https://www.linuxbabe.com/linux-server/install-coturn-turn-server-spreed-webrtc

* turnserver
apt-get install -t jessie-backport turnserver
tout est dans /usr/share/coturn

tls-listening-port=443
#listening-port=443
#alt-tls-listening-port=9443
#alt-listening-port=9443
fingerprint
lt-cred-mech
use-auth-secret
static-auth-secret=a3e80aedff21
realm=vs3.paquerette.eu
total-quota=100
bps-capacity=0
stale-nonce
no-loopback-peers
no-multicast-peers
cert=/etc/letsencrypt/live/vs3.paquerette.eu/fullchain.pem
pkey=/etc/letsencrypt/live/vs3.paquerette.eu/privkey.pem
cipher-list="ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AES:RSA+3DES:!ADH:!AECDH:!MD5"
no-stdout-log
simple-log
log-file=/var/log/turnserver.log
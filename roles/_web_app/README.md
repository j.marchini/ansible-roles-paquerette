# standard (web) application (static, php, python...)

- vars:
  - **app_program**: Program name
  - **app_run**: install / reinstall / upgrade / uninstall
  - **app_instance_id**: must be unique on a server
  - **app_version**: version, git branch if git repo is defined
  - app_old_version: used to identify backup when upgrade
  
  - app_domain: full domain name for the service
  
  - database_type: **None** cf role _create_database
  - database_name: **<app_instance_id>_db**
  - database_user: **<app_instance_id>_usr**
  - **database_password**: (mandatory if database type is not "None")
  
  - app_git_repo: git repository (if app_source is not used)
  - app_src: app source url for download (if app_git is not used)
  - app_src_root_name : root directory in archive  if app_source is used
  - local_release : **releases/{{ role_name }}/default** if the archive exists, then used instead of source location 
  - empty_slot: yes/**no**: when yes don't fetch any git repo or source and deploy only a basic index.html file
  
  - **app_user**:
  - app_user_chrooted: **yes**/no
  - app_user_key_file
  - app_user_password: cf note
  - app_group: **<app_user>**

  - app_instance_root: 
    - **<app_user_home>/<app_instance_id>** if app_user is defined
    - **<www_root>/<app_instance_id>** if no app_user is defined

  - instance_www_root: **<app_instance_root>**
  - packages_list: **[]** additional system packages to be installed 

  - php_composer: yes/**no**: when yes, uses composer in install and updates with basic command and standard options (nodev...)

  - python3: yes/**no**
  - python_module_list: **[]** additional modules to be installed via pip3
  - app_venv_entry_point 
  - app_wsgi: yes/**no**
  - app_wsgi_entry_point 

- to provide an encrypted password to ansible, use this :
    `python3 -m  pip install --user passlib`
    `python3 -c "from passlib.hash import sha512_crypt; import getpass; print(sha512_crypt.encrypt(getpass.getpass()))"`

- platform roles : 
    - _python3 or php7_fpm ...
    - **apache_server** (or nginx_server coming later) if needed (app_domain defined)
    - any database server

    set chroot_jail: "yes" in host var if ssh chroot is needed **experimental**
    
[paquerette.eu](http://paquerette.eu)

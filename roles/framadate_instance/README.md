# Application Framadate 

- vars : 
    - **app_domain**
    - **app_instance_id**
    - app_version: **master**
    - **app_old_version** : last app_version
    - **app_run** : skip / install / reinstall / upgrade
    - admin_user: **admin**
    - **admin_password**
    
- platform roles : 
    - apache2_server
    - mariadb_mysql_server
    - php7_fpm

after install, manually finish the configuration for smtp use in app/inc/config.php 

https://framagit.org/framasoft/framadate/framadate/-/wikis/Install/Configure


[paquerette.eu](http://paquerette.eu)

# Configuration of backup master  

backup master retrieves backups from slaves

requirements : 
 - \<None\>

vars:
 - **backup_slaves** : list of slaves
 
[paquerette.eu](http://paquerette.eu)
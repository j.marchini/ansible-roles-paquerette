# ansible-roles-paquerette

*Proposition of **minimal** ansible roles, to host and maintain services on premise or in the cloud.* 

Supported systems : **Ubuntu 16.04 LTS**, **Ubuntu 18.04 LTS**
partially : **debian 9**

Principles :

- "less is more" and "simpler is better"
- usage of Ansible for all good reasons
- minimisation of shell usage
- 1 configuration file per host with his complete definition and the list of instances
- 1 utility to manage inventory : inventory.py
- 1 utility to apply changes on servers : play.py
- possibility to recover backup on a separate dedicated machine, the master backup server

Technical choices:

- 2 physical partitions based configuration, 1 system, 1 programs and data, mounted on **/mnt/vdb** by default
- monitoring : **monit**
- backup : **backupninja**, external backup in "master/slave" mode: an external server is connecting to the host to recover backups
- mail, alters : **postfix as SMTP relay**, possibility of bcc
- **all** services are provided using HTTPS with **letsencrypt** certificates

## 0 - Prerequisites

- an ubuntu machine with public IP. (**the server**)
- a machine with ansible to manage the servers. (**the controller**) (It is also possible to use ansible locally on the server)
- on the server : optionally, recommended, a distinct partition for data and programs, mounted on /mnt/vdb by default. If absent, set **data_partition** to *no* or *False* in the host_vars 
- on the server : **an administrative account** which can sudo commands as root without providing password
- on the server: an ssh key for the master backup server (can be the same as the administrative account, but not recommended) 
- **a mail box** available via SMTP for alerts.

All relevant variables are in the group_vars base_server file, in PREREQUISITES sections

## 1 - The inventory
Here is the definition of the list of servers, for each server the composition of the platform and the list of instances and all role variables.

in groups_vars :
***In bold**, groups to be defined specifically, as your needs.* 
- secret : common secret variables (partner keys, smtp password...)
- - base_server : all common non secret variables
- - - **test** : all specifics variables for testings hosts
- - - **prod** : all specifics variables for production hosts

etc...

cf: [./doc/hosts.prod](./doc/hosts.prod)

## 2 - The server
Here is the definition of the base of the server, it implements :
- the base file and directory organization
- the server localization
- the backup strategy
- the monitoring strategy

cf: [./doc/host_template.yml](./doc/host_template.yml)

**rôle : [base_server](./roles/base_server/README.md)**

## 3 - The platform
Here is the definition of the components used to run the instances, it implements :  
- web server (nginx, apache,...)
- database server (mariadb, postgres, mongodb...)
- languages (php, python, nodejs...)
- associated monitoring

**role : [base_platform](./roles/base_platform/README.md)**

## 4 - Instance deployment, upgrade, removal, maintenance, restoration, etc...
Here is the implemented methods for the full live of an instance : 
for the deployment (install / reinstall command):
- download of the application
- database and dependencies management (letsencrypt certificate...)
- base configuration of the application
- backup and monitoring
- service start

for the upgrade (upgrade command):
- download of the new version
- service stop
- complete cold backup
- less or more automatic upgrading of the application
- service start

for the removal (uninstall command):
- stop backup and monitoring
- service stop
- program removal and database drop
- removal of associated tasks (logrotate...)
- revocation and deletion of the letsencrypt certificate

for the restoration (restore command):
- recovering of database and application files from archive, and restoration, except external data as users files for Nextcloud

**roles : \<application\>_instance**

## 5 - Reusable elements 

Roles are using factorized parts and reusable (database creation, certificates etc...)

**roles : \_app\_\<fonction\>**

## 6 - Instance management

The inventory is managed with the utility program **inventory.py**
The application of the changes is done with the utility program **play.py** which generates the playbook and runs ansible.

for example : 

    ./inventory.py --new-instance
    ./play.py myhost myinstance install
    ./play.py -e 'domain_name=www.peace.org' -r myhost letsencrypt_nightly_new

**utility programs : inventory.py, play.py**

## 7 - Utility roles

Getting letsencrypt certificates during the nightly shutdown: **letsencrypt_nightly_new** :

    ./play.py -e 'domain_name=www.peace.org' -r myhost letsencrypt_nightly_new

## 8 - Special cases
Some roles are specifics, as the Collabora instance management :

**roles :**
- **collabora\_online\_instance**
- **mumble_server** 
- **wekan_instance_snap** (no longer maintained)
- **turn\_server**
- **_master_backup_server** 

## Documentation :
cf: [The documentation of your dreams](./doc/README.md "And all your dreams come true !")

## Notes :

STABLES ROLES
- base server (_python3)
- base platform (apache, nginx, mysql/mariadb, mongodb, nodejs, postgres, php7_fpm )
- _app_log_inventory, _app_backup, _app_restore_instance, _app_logrotate, _app_monit, _create_database, _letsencrypt_certificate
- backup in master/slave mode
- nextcloud_instance (nginx, apache)
- collabora online_instance (official partner) (nginx, apache)
- dolibarr_instance (apache only) (no longer maintained)
- rocketchat_instance (no longer maintained) : unfortunately, it's now impossible to install several instances on the same host 
- _web_app (chrooted sftp only user, git, static, php, python) (empty, wordpress, grav, pelican, yeswiki, adminer etc...) (apache only)
- derived from _web_app : wordpress_instance, yeswiki_instance, adminer_instance
- mattermost_instance

NEW ROLES OR IN PROGRESS (may be subject to important refactoring):
- tryton_instance
- framadate_instance (install only)
- grav_instance, pelican_instance, 
- wekan snap (missing uninstall)(no longer maintained)
- turn_server
- mumble_server (install only beta)

OTHERS :
- status.py : utility providing a rapid check of the production

TODO : 
- database files in system partition : move /var/lib/ postgres.... in /mnt/vdb/ ... in study
- failtoban for services (or not)

to redesign : 

- wekan snap in backup role

[paquerette.eu](http://paquerette.eu "L'informatique responsable est l'affaire de tous !")

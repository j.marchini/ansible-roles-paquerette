#! /usr/bin/env python3
"""
11/05/2019 Jerome Marchini
paquerette.eu
"""

import sys
import argparse
import yaml
import os
import requests
import paramiko

# YAML configuration file
configuration_file = 'paquerette_utils.conf.yml'
# ansible hosts vars
host_vars_path = './host_vars'

# exception without traceback
sys.tracebacklimit = 0

#
default_http_timeout = 1.5
default_ssh_timeout = 1.5
# avoid logging
user_agent = 'Monit'


class AbortOnFail(Exception):
    pass


class Status:
    def __init__(self):
        self.args = None
        # self.host_vars = None
        # self.host_instances = {}
        self.status_request = {}
        self.ok_count = 0
        self.fail_count = 0
        self.remote_host = None
        self.remote_connexion = None
        self.ssh_timeout = default_ssh_timeout
        self.http_timeout = default_http_timeout
        # loading configuration
        with open(configuration_file, 'r') as stream:
            self.conf = yaml.load(stream, Loader=yaml.FullLoader)

    def list_hosts(self):
        hl = self.conf.get('host_list')
        if hl is not None:
            for h in hl:
                yield h
        else:
            for f in sorted(os.listdir(host_vars_path)):
                if f.endswith('.yml'):
                    yield f[:-4]

    @staticmethod
    def load_host_vars(_host):
        with open(os.path.join(host_vars_path, _host + '.yml'), 'r') as host_file:
            return yaml.load(host_file, Loader=yaml.FullLoader)

    # low level methods
    def ssh_connect(self, host, user=None):
        self.remote_connexion = paramiko.SSHClient()
        self.remote_connexion._policy = paramiko.WarningPolicy()
        self.remote_connexion.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_config = paramiko.SSHConfig()
        ssh_config_file = os.path.expanduser("~/.ssh/config")
        if os.path.exists(ssh_config_file):
            with open(ssh_config_file) as f:
                ssh_config.parse(f)
        cfg = {'hostname': host, 'username': user}
        ssh_host_config = ssh_config.lookup(cfg['hostname'])
        # lookup returns dictionary with 'hostname' key only
        if len(ssh_host_config) == 1:
            raise Exception('Hostname %s not found in ssh config file' % host)
        if user is None:
            cfg['username'] = ssh_host_config['user']
        else:
            cfg['username'] = user

        for k in ('hostname', 'username'):
            if k in ssh_host_config:
                cfg[k] = ssh_host_config[k]
        if 'identityfile' in ssh_host_config:
            cfg['key_filename'] = ssh_host_config['identityfile']
        if 'proxycommand' in ssh_host_config:
            cfg['sock'] = paramiko.ProxyCommand(ssh_host_config['proxycommand'])
        try:
            self.remote_connexion.connect(**cfg)
        except Exception as E:
            self.fail_count += 1
            print('Failed : Host : "%s" : ssh connection:' % host)
            print(E)
            raise AbortOnFail()

    def ssh_cmd(self, cmd):
        stdin, stdout, stderr = self.remote_connexion.exec_command(cmd, timeout=self.ssh_timeout)
        out = stdout.readlines()
        err = stderr.readlines()
        return out, err

    def ssh_tst_cmd(self, cmd, msg="", check=None, abort_on_fail=False):
        try:
            stdin, stdout, stderr = self.remote_connexion.exec_command(cmd, timeout=self.ssh_timeout)
            out = stdout.readlines()
            err = stderr.readlines()
            err_msg = None
            if len(err) > 0:
                err_msg = err[0]
            rc = ''
            if len(out) > 0:
                rc = out[0]
            if err_msg is None and (check is None or rc == check + '\n'):
                self.ok_count += 1
                if not self.args.quiet:
                    print('OK : ' + msg)
                return tuple((0, rc))
            else:
                self.fail_count += 1
                print('Fail : ' + msg)
                print(err_msg)
                if abort_on_fail:
                    raise AbortOnFail()
                return tuple((0, rc))
        except Exception as E:
            self.fail_count += 1
            print('Fail : ' + msg)
            print(E)
            if abort_on_fail:
                raise AbortOnFail()
            return tuple((-1, E))

    def instance_id_of(self, instance):
        """
        return instance_id of an instance
        :param instance:
        :return:
        """
        iit = self.conf.get('instance_id_token', None)
        if iit is None:
            raise Exception('instance_id_token not found in configuration file')
        return instance.get(iit)

    def get_instance_list(self, _host):
        """
        return the list of instances of a host
        :param _host:
        :return:
        """
        ilt = self.conf.get('instance_list_token', None)
        if ilt is None:
            raise Exception('instance_list_token not found in configuration file')
        with open(os.path.join(host_vars_path, _host + '.yml'), 'r') as host_file:
            host_vars = yaml.load(host_file, Loader=yaml.FullLoader)
            return host_vars.get(ilt)

    def get_instance(self, _host, _instance_id):
        """

        :param _host:
        :param _instance_id:
        :return:
        """
        for instance in self.get_instance_list(_host):
            if self.instance_id_of(instance) == _instance_id:
                return instance
        raise Exception("Instance %s not found in host %s" % (_instance_id, _host))

    def status_info(self, host):
        connected = False
        try:
            # remote connexion via ssh
            self.ssh_connect(host)
            connected = True
            # monit service
            out, err = self.ssh_cmd('df -h /')
            print(out)
            out, err = self.ssh_cmd('df -h /mnt/vdb')
            print(out)
            out, err = self.ssh_cmd('free -h')
            print(out)
            out, err = self.ssh_cmd('uptime')
            print(out)
        except Exception as E:
            if not connected:
                print('unable to open ssh connexion on host "%s":' % host)
                print(E)
        finally:
            if connected:
                self.remote_connexion.close()

    def status_host(self, host):
        connected = False
        try:
            # remote connexion via ssh
            self.ssh_connect(host)
            connected = True
            # monit service
            self.ssh_tst_cmd(
                'systemctl is-active monit',
                check="active",
                msg='monit on %s' % host)
            # postfix service
            self.ssh_tst_cmd(
                'systemctl is-active postfix',
                check="active",
                msg='postfix on %s' % host)
            # filesystem - if fails, pass
            self.ssh_tst_cmd(
                'sudo touch /mnt/vdb/.status_touch',
                msg='Data partition in ReadWrite mode on %s' % host,
                abort_on_fail=True)

            host_vars = self.load_host_vars(host)
            # database servers
            postgres = host_vars.get('postgres_server')
            if postgres == 'yes':
                self.ssh_tst_cmd(
                    'systemctl is-active postgresql',
                    check="active",
                    msg='Postgres on %s' % host)
            mysql = host_vars.get('mariadb_mysql_server')
            if mysql == 'yes':
                self.ssh_tst_cmd(
                    'systemctl is-active mysql',
                    check="active",
                    msg='Mysql / Mariadb on %s' % host)
            mongodb = host_vars.get('mongodb_server')
            if mongodb == 'yes':
                self.ssh_tst_cmd(
                    'systemctl is-active mongod',
                    check="active",
                    msg='Mongodb on %s' % host)
            # php platform
            php = host_vars.get('php_server')
            if php == 'yes':
                php_version = host_vars.get('php_version')
                self.ssh_tst_cmd(
                    'systemctl is-active php%s-fpm' % php_version,
                    check="active",
                    msg='Php(%s) on %s' % (php_version, host))
                # redis
                self.ssh_tst_cmd(
                    'systemctl is-active redis',
                    check="active",
                    msg='Redis on %s' % host)
            # reverse proxy
            rev_proxy = host_vars.get('rev_proxy', '')
            if rev_proxy != '':
                self.ssh_tst_cmd(
                    'systemctl is-active %s' % rev_proxy,
                    check="active",
                    msg='%s on %s' % (rev_proxy, host))
        except AbortOnFail:
            pass
        finally:
            if connected:
                self.remote_connexion.close()

    def status_instance(self, _host, _instance):
        if _instance.get('app_domain') is None:
            return
        headers = {'User-Agent': user_agent}
        if len(_instance.get('app_domain', '')) == 0:
            return
        url = 'https://'+_instance['app_domain']
        try:
            response = requests.get(url, headers=headers, timeout=self.http_timeout)
            if response.status_code != 200:
                raise Exception("HTTP status code %s" % response.status_code)
            if not self.args.quiet:
                print('OK : Host: "%s" Instance "%s" Elapsed : %s url: %s'
                      % (_host, self.instance_id_of(_instance), str(response.elapsed), url))
            self.ok_count += 1
        except Exception as E:
            self.fail_count += 1
            print('Failed : Host: "%s" Instance "%s" url: %s' % (_host, self.instance_id_of(_instance), url))
            print(E)

    def run_status(self):
        if self.args.status_host:
            for h in self.status_request['hosts']:
                self.status_host(h)
                if self.args.info:
                    self.status_info(h)
        if self.args.status_instance:
            for i in self.status_request['instances']:
                self.status_instance(i['host'], i['instance'])
        if self.fail_count != 0 or not self.args.quiet:
            print('OK : %s Failed : %s' % (self.ok_count, self.fail_count))

    def parse_args(self):
        parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.description = \
            "prints in stdout full status\n\n" + \
            "Gathering status informations on running hosts and instances :\n" + \
            "- using configuration file (see for details): %s" % configuration_file

        parser.add_argument("host", nargs='?', help='host')
        parser.add_argument("instance", nargs='?', help='instance')
        parser.add_argument("-i", "--info", help="return info on host (memory, disk, etc...)", action="store_true",
                            default=False)
        parser.add_argument("-q", "--quiet", help="quiet mode, report only errors", action="store_true", default=False)
        parser.add_argument("-s", "--status-instance", help="status of instances running on host", action="store_true",
                            default=False)
        parser.add_argument("-S", "--status-host", help="status of host's platform components", action="store_true",
                            default=False)
        parser.add_argument("--http-timeout", help="timeout for http requests (default : %s)" % default_http_timeout,
                            default=default_http_timeout)
        parser.add_argument("--ssh-timeout", help="timeout for ssh requests (default : %s)" % default_http_timeout,
                            default=default_ssh_timeout)
        self.args = parser.parse_args()

        # when no host or instance specified, all included
        if not self.args.status_host and not self.args.status_instance:
            self.args.status_host = True
            self.args.status_instance = True

        self.ssh_timeout = float(self.args.ssh_timeout)
        self.http_timeout = float(self.args.http_timeout)

    # top level
    def run(self):
        self.parse_args()

        # prepare status request
        if self.args.host:
            # if host is given in args
            self.status_request['hosts'] = [self.args.host]
        else:
            # all hosts in config file
            self.status_request['hosts'] = self.conf["host_list"]

        self.status_request['instances'] = []
        if self.args.instance:
            # if given in args
            self.status_request['instances'] = [self.args.instance]
        else:
            # all instances for host(s)
            for h in self.status_request['hosts']:
                il = self.get_instance_list(h)
                if il is not None:
                    for i in il:
                        self.status_request['instances'].append({'host': h, 'instance': i})
        self.run_status()
        exit(self.fail_count)


if __name__ == '__main__':
    Status().run()

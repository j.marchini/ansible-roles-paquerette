#! /usr/bin/env python3
"""
15/03/2019 Jerome Marchini
paquerette.eu
"""

import sys
import os
import subprocess
import yaml
import argparse
from jinja2 import Template

# YAML configuration file
configuration_file = 'paquerette_utils.conf.yml'
host_vars_path = './host_vars'

sys.tracebacklimit = 0


class Playbook:
    def __init__(self):
        self.host = None
        self.roles = []
        self.vars = {}
        self.become = True

    def content(self):
        for k in self.vars.keys():
            if isinstance(self.vars[k], str):
                self.vars[k] = '"%s"' % self.vars[k]
        pl_template = \
            "---" +\
            "\n\n- hosts: {{ host }}" +\
            "{% if vars %}" +\
            "\n\n  vars:" +\
            "{% for key, value in vars.items() %}" +\
            "\n    {{ key }}: {{ value }}" +\
            "{% endfor %}" +\
            "{% endif %}" +\
            "\n\n  roles:" +\
            "{% for role in roles %}" +\
            "\n    - {{ role }}" +\
            "{% endfor %}" +\
            "\n\n  become: {{ become }}"

        return Template(pl_template).render(vars(self))


class Play:

    def __init__(self):
        self.args = None
        # loading configuration
        with open(configuration_file, 'r') as stream:
            self.conf = yaml.load(stream, Loader=yaml.FullLoader)
        self.run_playbook = True
        self.playbook = Playbook()
        self.playbook_file_name = self.conf['playbook_file_name']

    # facilities
    # ui facilities
    @staticmethod
    def ui_confirm(msg, default):
        """
        prompt with message and return true or false or default if no response
        :param msg:
        :param default:
        :return:
        """
        valid = ['y', 'Y', 'n', 'N']
        if default:
            msg = msg + ' (Y/n) ? '
            not_expected = ['n', 'N']
        else:
            msg = msg + ' (y/N) ? '
            not_expected = ['y', 'Y']
        i = ''
        while i not in valid:
            i = input(msg)
            if i == '':
                break
        if i in not_expected:
            return not default
        else:
            return default

    @staticmethod
    def ui_select(choices, prompt=None, confirm_message=None, refresh=None, mandatory=False, default_index=None,
                  default_confirm=True):
        """
        loop until user makes a selection between choices
        :param choices: liste of choices
        :param prompt: prompt string
        :param confirm_message:
        :param refresh: callback (choices : None)
        :param mandatory: cannot exit without selection
        :param default_index: from 1 to len(choices)
        :param default_confirm: default confirmation if confirm message is set on
        :return: index selected from 0 to len(choices) -1
        """
        if prompt is None:
            prompt = "Please select into :"
        if len(choices) == 0:
            if mandatory:
                raise Exception("%s : no choices valid" % prompt)
            else:
                return None
        print(prompt)
        selected = None
        while selected is None:
            for i, c in enumerate(choices):
                print("{0} - {1}".format(i + 1, c))
            # set select prompt
            select_prompt = 'Select (1-%d)' % (len(choices))
            if refresh:
                select_prompt = select_prompt + ' (r=refresh)'
            if default_index:
                select_prompt = select_prompt + ' [default=%s]' % default_index
            selected = input("%s ? " % select_prompt)
            # default
            if selected == '' or selected is None:
                if default_index:
                    selected = str(default_index)
                else:
                    selected = None
                if selected is None:
                    if mandatory:
                        continue
                    else:
                        break
            # refresh
            if refresh and selected in ['r', 'R']:
                selected = None
                default_index = None
                refresh(choices)
                continue
            # validate choice
            if not selected.isdigit() or int(selected) > len(choices) or int(selected) < 1:
                print("Selection is not valid, please retry")
                selected = None
                continue
            # confirm choice
            if confirm_message:
                if selected:
                    c = Play.ui_confirm('{0} "{1}"'.format(confirm_message, choices[int(selected) - 1]),
                                        default_confirm)
                else:
                    c = Play.ui_confirm('{0} none'.format(confirm_message), default_confirm)
                if not c:
                    selected = None
                    continue
        # return None or 'natural' selected index : 0 - len(choices)-1
        if selected:
            return int(selected) - 1
        else:
            return None

    # low level methods
    def instance_id_of(self, instance):
        """
        return instance_id of an instance
        :param instance:
        :return:
        """
        iit = self.conf.get('instance_id_token', None)
        if iit is None:
            raise Exception('instance_id_token not found in configuration file')
        return instance.get(iit)

    @staticmethod
    def get_host_description(_host):
        with open(os.path.join(host_vars_path, _host + '.yml'), 'r') as host_file:
            host_vars = yaml.load(host_file, Loader=yaml.FullLoader)
            return host_vars.get('description', '')

    def get_instance_list(self, _host):
        """
        return the list of instances of a host
        :param _host:
        :return:
        """
        ilt = self.conf.get('instance_list_token', None)
        if ilt is None:
            raise Exception('instance_list_token not found in configuration file')
        with open(os.path.join(host_vars_path, _host + '.yml'), 'r') as host_file:
            host_vars = yaml.load(host_file, Loader=yaml.FullLoader)
            return host_vars.get(ilt, [])

    def get_instance(self, _host, _instance_id):
        """

        :param _host:
        :param _instance_id:
        :return:
        """
        for instance in self.get_instance_list(_host):
            if self.instance_id_of(instance) == _instance_id:
                return instance
        raise Exception("Instance %s not found in host %s" % (_instance_id, _host))

    def make_interactive(self):
        """
        prepare playbook in interactive mode
        :return:
        """
        def complete_cmd_with_opt_args(_cmd):
            if self.args.list_tags:
                _cmd += " --list-tags"
            if self.args.apply_tags:
                _cmd += ' --tags "%s"' % self.args.apply_tags
            if self.args.skip_tags:
                _cmd += ' --skip-tags "%s"' % self.args.skip_tags
            if self.args.extra_vars:
                _cmd += ' --extra-vars "%s"' % self.args.extra_vars
            return _cmd

        hl = self.conf["host_list"]
        h_choices = ['{0} ( {1} )'.format(h, self.get_host_description(h)) for h in self.conf["host_list"]]
        sh = hl[self.ui_select(h_choices, prompt='Select an host :', mandatory=True)]
        self.playbook.host = sh
        il = self.get_instance_list(self.playbook.host)
        i_choices = None
        s = 0
        if il is not None:
            i_choices = ['{0} ( {1} )'.format(self.instance_id_of(i), i.get('description', '')) for i in il]
            i_choices.append("or select a role ...")
            s = self.ui_select(i_choices, prompt='Select an instance :', default_index=len(i_choices))
        role_mode = (il is None) or (s == len(i_choices)-1)
        if role_mode:
            rl = self.conf["role_list"]
            sr = rl[self.ui_select(rl, prompt='Select a role :', mandatory=True)]
            self.playbook.roles.append(sr)
            cmd = complete_cmd_with_opt_args("run %s -r %s %s" % (sys.argv[0], sh, sr))
            self.run_playbook = \
                self.args.list_tags or \
                self.ui_confirm(cmd, False)
        else:
            sr = il[s].get(self.conf.get('role_name_token'))
            self.playbook.roles.append(sr)
            self.playbook.vars = il[s]
            cl = self.conf["command_list"]
            cs = cl[self.ui_select(cl, prompt='Select an command :', mandatory=True)]
            self.playbook.vars[self.conf.get('command_name_token')] = cs
            cmd = complete_cmd_with_opt_args("run %s %s %s %s" % (sys.argv[0], sh, self.instance_id_of(il[s]), cs))
            self.run_playbook = \
                self.args.list_tags or \
                self.ui_confirm(cmd, False)

    def make_from_args(self):
        """
        prepare playbook according to args
        :return:
        """
        self.playbook.host = self.args.host
        if self.args.item:
            if self.args.role:
                self.playbook.roles.append(self.args.item)
            else:
                if not self.args.command:
                    raise Exception("Command must be defined when running on an instance")
                instance = self.get_instance(self.playbook.host, self.args.item)
                self.playbook.roles.append(instance.get(self.conf.get('role_name_token')))
                self.playbook.vars = instance
                self.playbook.vars[self.conf.get('command_name_token')] = self.args.command
        if not (self.args.yes or self.args.list_tags):
            self.run_playbook = self.ui_confirm('run playbook {0}'.format(self.playbook_file_name), False)

    # high level methods
    def make_playbook(self):
        if self.args.playbook_name:
            self.playbook_file_name = self.args.playbook_name
        self.playbook.become = self.conf.get('become', True)
        if self.args.interactive:
            self.make_interactive()
        else:
            self.make_from_args()
        with open(self.playbook_file_name, mode='w') as y:
            y.write(self.playbook.content())

    def schedule_at(self):
        if self.args.command:
            job_name = "{0}_{1}_{2}" .format(self.args.host, self.args.item, self.args.command)
        elif self.args.item:
            job_name = "{0}_{1}" .format(self.args.host, self.args.item)
        else:
            job_name = self.args.host

        at_args = '-y -p {0} {1}'.format(job_name+'.yml', self.args.host)
        if self.args.item:
            at_args += ' ' + self.args.item
        if self.args.role:
            at_args = '-r ' + at_args
        if self.args.command:
            at_args += ' ' + self.args.command
        if self.args.apply_tags:
            at_args += " --apply-tags " + self.args.apply_tags
        if self.args.skip_tags:
            at_args += " --skip-tags " + self.args.skip_tags
        if self.args.extra_vars:
            at_args += " --extra-vars " + self.args.extra_vars
        at_args += " >> {0}.log" .format(job_name)

        play_cmd = subprocess.Popen(["echo", "./play.py {0}".format(at_args)], stdout=subprocess.PIPE)
        at_cmd = subprocess.Popen(["at", self.args.at_time], stdin=play_cmd.stdout, stdout=subprocess.PIPE)
        play_cmd.stdout.close()
        exit(at_cmd.wait())

    def script_command(self):
        for h in self.conf["host_list"]:
            for i in self.get_instance_list(h):
                i['host'] = h
                i['command'] = self.args.script_command
                print(Template(self.conf['script_default_template']).render(i))

    def launch_playbook(self):
        cmd = ["ansible-playbook", self.playbook_file_name]
        if self.args.list_tags:
            cmd.append("--list-tags")
        if self.args.apply_tags:
            cmd.extend(["--tags", self.args.apply_tags])
        if self.args.skip_tags:
            cmd.extend(["--skip-tags", self.args.skip_tags])
        if self.args.extra_vars:
            cmd.extend(["--extra-vars", self.args.extra_vars])
        print(' '.join(cmd))
        rc = subprocess.call(cmd)
        return rc

    def neutralize_playbook(self):
        os.unlink(self.playbook_file_name)

    def parse_args(self):
        parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.description = \
            "prepares an ansible playbook and run a role\n\n" + \
            "play.py -i : interactive mode\n\n" + \
            "play.py [myhost|mygroup] : runs empty playbook (gathering facts) on target\n\n" + \
            "running a role :\n" + \
            "play.py -r [myhost|mygroup] myrole: runs myrole on target\n\n" + \
            "can be used to run on an instance (object in a list of instance in the specified host_vars file) " + \
            "according rules fixed in configuration file :\n" + \
            "play.py myhost myinstance mycommand : runs defined role with defined command on instance target\n"

        parser.add_argument("host", nargs='?', help='host to run the role, or group if no instance is defined')
        parser.add_argument("item", nargs='?', help='instance to run the role and command on, role if r option is set')
        parser.add_argument("command", nargs='?',
                            help='command to run on instance : install, reinstall, upgrade, uninstall')
        parser.add_argument("--apply-tags", help="applying tags")
        parser.add_argument("--skip-tags", help="skipping tags")
        parser.add_argument("--list-tags", help="list tags (dry run only for tag listing, includes 'yes' mode)",
                            action="store_true", default=False)
        parser.add_argument("-a", "--at-time", help="scheduling with at command at at_time")
        parser.add_argument("-e", "--extra-vars", help="applying extra vars")
        parser.add_argument("-i", "--interactive", help="interactive mode", action="store_true", default=False)
        parser.add_argument("-p", "--playbook-name",
                            help="name of playbook file name (by default : playbook_file_name in configuration)")
        parser.add_argument("-r", "--role", help='"item" argument as a role, ignoring "command" argument',
                            action="store_true", default=False)
        parser.add_argument("-s", "--script-command", help="make a script on all instances with a SCRIPT_COMMAND")
        parser.add_argument("-y", "--yes", help="launch ansible without prompt (not in interactive) ",
                            action="store_true", default=False)
        self.args = parser.parse_args()
        if not (self.args.interactive or self.args.host or self.args.script_command):
            parser.print_usage()
            exit(1)

    # top level
    def run(self):
        """
        main
        :return:
        """
        self.parse_args()
        if self.args.script_command:
            self.script_command()
            exit(0)
        self.make_playbook()
        if self.args.at_time:
            self.schedule_at()
        if self.run_playbook:
            rc = self.launch_playbook()
            self.neutralize_playbook()
            if rc == 0:
                print("success")
            else:
                print("playbook failed with return code %d" % rc)
            exit(rc)


if __name__ == '__main__':
    Play().run()

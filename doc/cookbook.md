Basic actions, for more advanced see ./advanced_stuffs.md

### Inventory

**List all inventory :**

    ./inventory.py

**List hosts :**

    ./inventory.py -L

**List instances of a host:**

    ./inventory.py -l myhost

or in interactive mode :

    ./inventory.py -l

**Filtrer to find something in inventory :**

    ./inventory.py -f mydomain.org

**Print information on an instance :**

    ./inventory.py -i myhost myinstance

or interactive : 

    ./inventory.py -i

output example :

    Definition of myinstance on myhost :
    role: wordpress_instance
    description: wordpress instance of mydomain.org
    app_domain: myfomain.org
    app_instance_id: wp_mydomain
    database_password: dAH451a.?1qd*
    app_user: mydomain_user
    clear_app_user_password: d42?.8vyd9Rc4
    app_user_password: $6$jDAdbjKOFVTgGhXP$R8zHbaYnqNNQ/TMOMfTOhBJ56S3g.DpO/yyhDufgQy5zZctdAqLBi5Rhk9kbnmgj4va2S9859EBf7BIoL7DU80

see also "preparing a message" for custom formatting


### Get a new letsencrypt certificate during the nightly shutdown
use **./play.py** to play the role **letsencrypt_nightly_new** on **myhost** with the "extravar" **mydomain.org**

    ./play.py -r myhost letsencrypt_nightly_new -e 'domain_name=mydomain.org

### After that, install an instance on a host
**Note :** If the certificate is not present on the host, all services will be stopped (for 15 seconds) to get the certificate

First, use **./inventory.py** to add the instance in the inventory in interactive mode
    
    ./inventory.py -n

Answers the questions...

Then, install it on the server :

    ./play.py myhost myinstance install

if it fails some some reason (network...)
retry like this :

    ./play.py myhost myinstance reinstall

**install** command checks that the instance is **not present** on the host to avoid **replace an instance by another**.
**reinstall** command checks the instance is **present** on the host for the same reason

### After that, preparing a message for credentials or another stuff
    ./inventory.py -m

will print a message generated from **message_default_template** in configuration file with instance informations

### Upgrade an instance

First, use **./inventory.py** to change the version of the instance in the inventory in interactive mode

The upgrade process will create first a full backup of the instance in backup dir named "app_old_version"

**NOTE if the slot for app_old_version already exists, the backup is skipped.  This is the condition for having a reliable backup procedure that can be ran again if something fails, until it succeeds.** 

Then, upgrade it on the server :

    ./play.py myhost myinstance upgrade

### Remove an instance

First uninstall it on the server :

    ./play.py -e 'app_instance_to_uninstall=myinstance' myhost myinstance uninstall

"app_instance_to_uninstall=myinstance user_to_remove=myuser" is just a double check, to be sure you really want to uninstall it.

**Note** : 
* If The instance is removed from the inventory before applying on the server, you must recovery the instance informations from the backup of the host_vars file 
* **Backups are not destroyed when an instance is removed.**
* User is not removed unless the extra var **user_to_remove** is set with the correct user name. (many instances may be installed for the same user)

Then, remove it on inventory :

    ./inventory.py -r myhost myinstance

### Touch an instance, change informations in inventory and apply changes
    ./inventory.py -t myhost myinstance

You will be prompted for changes. If any changes can be applied on the server, run ./play.py host instance reinstall

If the domain name is changed, then finally run the role cleanup_old_domain_name to remove old certificates and monitoring this way:

    ./play.py -r myhost _cleanup_old_domain_name -e 'app_domain=myolddomain'

### Touching playbook before running it (maybe useful in some cases, keeping control on playbook)
Answers **NO** to **"run playbook (y/N) ?"** when using ./play.py. The playbook file **play.book.yml** is generated, you can touch it and then simply launch it with ansible :
    ansible-playbook play.book.yml



Considering you have a server called HOST with a user named ubuntu, bionic server is recommended.  
As every service runs with HTTPS protocol, you **must have** a **DNS A** rule with the **DOMAIN NAME of your service and the PUBLIC IP of your host**.
   
Now, en avant Simone :

On controller machine, the machine from where you control your host :

* Prerequisites

        sudo apt update
        sudo apt install software-properties-common
        sudo apt-add-repository --yes --update ppa:ansible/ansible
        sudo apt install ansible git python3 python3-yaml python3-jinja2
        
        # prepare the key for authentification on host
        mkdir $HOME/.ssh/paquerette
        ssh-keygen -f $HOME/.ssh/paquerette/id_rsa -N ''
        chmod 0600 $HOME/.ssh/paquerette/*
        ssh-copy-id -i "$HOME/.ssh/paquerette/id_rsa.pub" ubuntu@YOUR HOST IP
        
        # open a session on host
        ssh -i "$HOME/.ssh/paquerette/id_rsa" ubuntu@YOUR HOST IP
        echo 'ubuntu ALL=(ALL:ALL) NOPASSWD:ALL' | sudo EDITOR='tee -a' visudo
        sudo mkdir -p /mnt/vdb
        sudo apt install python
        # disconnect from host
        ^D
        
        # test correct configuration and keys, must return 'root' 
        ssh -i "$HOME/.ssh/paquerette/id_rsa" ubuntu@YOUR HOST IP sudo whoami
        # cloning the roles and facilities 
        git clone https://gitlab.com/j.marchini/ansible-roles-paquerette.git ~/ansible
    
        # copy the ansible hosts file:
        cp ~/ansible/doc/hosts.first ~/ansible/hosts
        # edit the hosts files to set YOUR HOST IP
        nano ~/ansible/hosts
    
        # replace and link /etc/ansible/hosts to our file
        sudo mv /etc/ansible/hosts /etc/ansible/hosts.save
        sudo ln -s ~/ansible/hosts /etc/ansible/hosts 
    
        # link /etc/ansible/roles to our roles
        sudo ln -s ~/ansible/roles /etc/ansible/roles 
    
        # copy paquerette_utils.conf.yml for inventory and play facilities
        cp ~/ansible/doc/paquerette_utils.conf.yml ~/ansible/
        
        # copy the host_vars file
        mkdir ~/ansible/host_vars && cp ~/ansible/doc/myfirsthost.yml  ~/ansible/host_vars/
        
        # test host configuration, must return 'myfirsthost'
        cd ~/ansible
        ./inventory.py -L  
    
        # edit the host_vars files to set your correct SMTP CREDENTIALS and application DOMAIN NAME
        nano ~/ansible/host_vars/myfirsthost.yml
    
        # test overall configuration, it must run "gathering facts" flawlessly on the server
        ~/ansible/play.py -ry myfirsthost

* Deploy the platform

        # deploy base configuration
        ./play.py -r myfirsthost base_server
        
        # deploy reverse proxy, database engine ...
        ./play.py -r myfirsthost base_platform

* Deploy the instance

        ./play.py myfirsthost myfirstinstance install

    or in interactive mode :

        ./play.py -i
        
    if something fails (letsencrypt, application download etc..) then run :
    
        ./play.py myfirsthost myfirstinstance reinstall
     
     until the process ends with no failure

ET VOILA !
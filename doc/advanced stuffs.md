## Advanced stuffs and things you will have to do soon or late... or never

### Advanced running facilities

#### scheduling playbook
using **at** command

at command must be installed on controller :

    sudo apt install at
    # ensure at is running
    sudo atd

use the --at-time option (see at manual for syntax)

    ./play.py myhost myinstance mycommand **-a 05:00**

the script logs in the file : myhost_myinstance_mycommand.log

#### making scripts 'walking' on instances
using **--script-command**

It is possible to prepare a script to apply changes on a set of instance :

    ./play.py -s mycommand
    
prints on stdout a list of ./play calls that you can filter :

    ./play.py -s upgrade | grep myhost

or decorate... 

    ./play.py -s 'maintenance -a 05:00' | grep nextcloud_instance > myscript.sh

... you can adapt the default template **script_default_template** in the configuration file to your needs.

#### using local releases to avoid download problems
It is possible to deploy releases from controller machine instead of trying to download them from external repository. 
To do that, just store the archive in **releases/{{ role_name }}/default** to deploy it.   
eg : 

    ls ./releases/wordpress_instance/
    lrwxrwxrwx 1 jerome jerome       13 juin  19 10:59 default -> latest.tar.gz
    -rw-rw-r-- 1 jerome jerome 11200591 juin  18 19:52 latest.tar.gz



- local_release : **releases/{{ role_name }}/default** (app_src must be set, set to "None" or "Local" for local only applications)
  

### Backup, restoration, recovery
[README.md](../roles/_app_restore_instance/README.md)

*NB* : **Nextcloud, and other applications are using and external data directory**, the backup and restoration of **user files are not covered by standard backup**. the reason is the volume is too big for that. If the files are critical, then find another solution more adapted for that like rclone based solution.
#### setup a remote backup server
using **_master_backup_server** role

It is nice to have an independent server with backups of your instances, strongly recommended for production environment
The master backup server uses ssh to connect to the production host with a key by default in ".ssh/backup_master", see *(
**master_backup_key_path** and master_backup_key_file )* 

setup the host_var **backup_slaves** : backup_slaves: ["myhost1", "myhost2",] etc

    ./play.py -ry mymasterbackuphost base_server
    ./play.py -ry mymasterbackuphost _master_backup_server

#### find all archives containing an instance and all "version backup" slots
using **_app_restore_instance** role or **restore command** and **restore_action=list**

"version backup" are made first when the upgrade command is called on an instance  

find an archive containing the instance on the host :

    ./play.py myhost myinstance restore -e 'restore_action=list'

find an archive containing the instance on the backup master host :

    ./play.py -ry masterbackuphost _app_restore_instance -e 'restore_action=list app_host=myhost'

#### after a crash
using **restore_action=restore**

to restore an instance, ensure it is deployed (empty):

    ./play.py myhost myinstance install

then use the restore action using archive previously found as myarchive.tar.gz as explained above: 

-case 1: from the same host

    ./play.py myhost myinstance restore -e 'restore_action=restore restore_source=myarchive.tar.gz'
    
-case 2 : from the controller :first copy the recovered archive to the host and restore it locally

    ./scp my_archive.tar.gz ubuntu@myhost:/tmp/my_archive.tar.gz
    ./play.py myhost myinstance restore -e 'restore_action=restore restore_source=/tmp/myarchive.tar.gz from_full_archive=no'

*NB* : if _app_restore_instance role is not integrated in the "instance role" extra vars must be provided by hand

#### installing an instance cloning another instance
using **from_instance_id**

It can be very useful to migrate files and database from an instance to another one. It is possible to clone an instance like this :

    ./play.py myhost myinstance_new install
    ./play.py myhost myinstance_new restore -e 'restore_action=restore restore_source=myarchive.tar.gz from_instance_id=myinstance"

*NB* : remember that **external data directory is not covered by standard backup**, migrating data must be done by hand

#### recover file and database of an instance
using **restore_action=recover**

first find the archive as explained above, then

    ./play.py myhost myinstance restore -e 'restore_action=recover restore_source=myarchive.tar.gz'

or 

    ./play.py -ry masterbackuphost _app_restore_instance -e 'restore_action=recover restore_source=myarchive.tar.gz app_instance_id=myinstance'

#### restoring after problematic upgrade
using **from_version_backup=yes**

    /play.py -y my_host myinstance restore -e 'restore_action=restore restore_source=/mnt/vdb/backups/version/myinstance/oldversion from_version_backup=yes'

### writing new roles
The best way to write new roles to provide new applications with less effort is to rely on reusable roles.
If the application is standard, use the [_web_app](../roles/_web_app/README.md) role and complete it as you need. 

See [wordpress instance vars](../roles/wordpress_instance/vars/main.yml) as example of reusing _web_app without any code.

### customizing inventory tools
coming soon...

### upgrade system host
coming soon...